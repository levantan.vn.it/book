// Load header / navigation bar
// Load registration form and sign in form

$(function()
{
    $("#header").load("Fixed_Navigation_Bar.html");
    $("#regiModal").load("Fixed_Registration.html");
    $("#signInModal").load("Fixed_SignIn.html");

})

//REGISTRATION/ SIGN UP FORM 
//1. ABN number will appear if "Host" is sellected/  Javascript

function displayABN(id, elementValue)
    {
        document.getElementById(id).style.display = elementValue.value==2 ? 'block' : 'none';
    }


// 2. Validate input Password/  JQUERy
$(document).ready(function()
    {
        // When users click "register" (submit) button, the form with the class ="regiForm" will validate the field.
        $('.regiForm').on('submit',function()
               {
                    //Check if the user sellect host or client yet?
                    if($('#inputClientHost option:selected').val()=="")
                    {
                        $('#regiMsg').html('Please sellect Client or Host!').css('color', 'red');
                        return false;
                    }
                    
                    else if($('#inputClientHost option:selected').val()=="1")
                    {
                         //Check if the confirm password #regiConfirmPassword == #regiPassword
                        if($('#regiConfirmPassword').val()!=$('#regiPassword').val())
                        {
                            $('#regiMsg').html("Error!:Password does not match").css('color', 'red');
                            return false;
                        }
                        else
                        {
                            alert("Your form has been successfully submited");
                            return true;
                        }
                    }
                   
                    // Check if ABN number is entered if host is chosen
                    else ($('#inputClientHost option:selected').val()=="2")
                    {
                        if($('#ABNNumber').val()=="")
                        {
                            $('#regiMsg').html("Error!: Please enter your ABN number").css('color', 'red');
                            return false;
                        }
                        //Check if the confirm password #regiConfirmPassword == #regiPassword
                        if($('#regiConfirmPassword').val()!=$('#regiPassword').val())
                        {
                            $('#regiMsg').html("Error!:Password does not match").css('color', 'red');
                            return false;
                        }
                        else
                        {
                            alert("Your form has been successfully submited");
                        }
                    }
                    
                } 
        );
    }
);

// "required" attribute will be used within html file so the following code to check 
 // if users enter some certain fields are not necessary


                    // //Check if First Name, Last Name, Email, Number, Password are entered
                    // else if($('#firstName').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!:You must enter your First Name").css('color', 'red');
                    //     return false;
                    // }
                    // else if($('#lastName').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!:You must enter your Last Name").css('color', 'red');
                    //     return false;
                    // }
                    // else if($('#regiEmail').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!:You must enter your Email").css('color', 'red');
                    //     return false;
                    // }
                    // else if($('#mobileNumber').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!:You must enter your Phone Number").css('color', 'red');
                    //     return false;
                    // }

                    // // Check if user enters password
                    // else if($('#regiPassword').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!:You must enter your password").css('color', 'red');
                    //     return false;
                    // }

                    // // Check if password is in right format
                    // else if($('#regiPassword').val()!="(?=.*\d)(?=.*[0-9])(?=.*[!@#%])(?=.*[a-z])(?=.*[A-Z]).{6,12}")
                    // {
                    //     $('#regiMsg').html("Error!: Password must contain at least 1 lower case letter, 1 uppercase letter, 1 number, one of following special characters ! @ # $ % and between 6 and 12 characters").css('color', 'red');
                    //     return false;
                    // }
                    

                    // // Check if address is entered
                    // else if($('#address').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!: Please enter your address").css('color', 'red');
                    //     return false;
                    // }

                    // // Check if suburd is entered
                    // else if($('#suburd').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!: Please enter your suburd").css('color', 'red');
                    //     return false;
                    // }

                    // // Check if postcode is entered
                    // else if($('#suburd').val()=="")
                    // {
                    //     $('#regiMsg').html("Error!: Please enter your suburd").css('color', 'red');
                    //     return false;
                    // }
                    
                    // // Check if state is chosen
                    // else if($('#state').val()=="0")
                    // {
                    //     $('#regiMsg').html("Error!: Please choose your state").css('color', 'red');
                    //     return false;
                    // }
// Check if check out date is after check in date
$(document).ready(function()
    {
        $('#searchForm').on('submit',function()
               {
                    if($('#checkInDate').val() > $('#checkOutDate').val() )
                    {
                        $('#searchMsg').html('Check-out date has to be after check in date').css('color', 'white');
                        return false;
                    }
                    
                    else
                    {
                        $('#searchMsg').html('Here are accommodations that match your search').css('color', 'white');
                        return false
                    }
                    
                } 
        );
    }
);
