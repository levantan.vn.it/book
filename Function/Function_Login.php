<?php
include 'Function_DataConn.php';
include 'Function_Session.php';

//retrieve email and password
if (isset($_POST)) {
    $user_email = trim($_POST['email']);
    $user_password = trim($_POST['password']);
    $user_type = $_POST['user_type'];


    //query from database based on the retrieved email
    if ($user_type == 1) {
        $sql = "SELECT * FROM client WHERE client_email='$user_email'";
        $result = $mysqli->query($sql);
        if($result){
            //convert the result to array (the key of the array will be the column names of the table)
            $row = mysqli_fetch_array($result);

            if ($row['client_email'] != $user_email) {
                echo "We cannot find your email";
            } else {
                // check the password
                if ($row['client_password'] == $user_password) {
                    // If the user name and password are matched with database
                    $_SESSION['session_id'] = $row['client_ID'];
                    $_SESSION['session_email'] = $row['client_email'];
                    $_SESSION['session_type'] = $row['client'];
                    // $_SESSION['session_access'] = $row['access'];
                    echo "client";
                    // header('Location: Client.php');

                } else {
                    echo "Invalid password";
                }
            }
        }else{
            echo "Invalid account";
        }
    } elseif ($user_type == 2) {
        $sql = "SELECT * FROM host WHERE host_email='$user_email'";
        $result = $mysqli->query($sql);
        if($result){
            //convert the result to array (the key of the array will be the column names of the table)
            $row = mysqli_fetch_array($result);

            if ($row['host_email'] != $user_email) {
                echo "We cannot find your email";
            } else {
                // check the password
                if ($row['host_password'] == $user_password) {
                    // If the user name and password are matched with database
                    $_SESSION['session_id'] = $row['host_ID'];
                    $_SESSION['session_email'] = $row['host_email'];
                    $_SESSION['session_type'] = $row['host'];

                    echo "host";
                } else {
                    echo "Invalid password";
                }
            }
        }else{
             echo "Invalid account";
        }
        

    } elseif ($user_type == 3) {
        $sql = "SELECT * FROM system_manager WHERE sm_email='$user_email'";
        $result = $mysqli->query($sql);
        if($result){
            //convert the result to array (the key of the array will be the column names of the table)
            $row = $result->fetch_array(MYSQLI_ASSOC);

            if ($row['sm_email'] != $user_email) {
                echo "We cannot find your email";
            } else {
                // check the password
                if ($row['sm_password'] == $user_password) {
                    // If the user name and password are matched with database
                    $_SESSION['session_id'] = $row['sm_ID'];
                    $_SESSION['session_email'] = $row['sm_email'];
                    $_SESSION['session_type'] = $row['manager'];
                    // $_SESSION['session_access'] = $row['access'];
                    echo "system manager";
                } else {
                    echo "Invalid password";
                }
            }
        }else{
            echo "Invalid account"; 
        }
    } else {
        echo "Invalid account";
    }
}
