<!-- REGISTRATION/ SIGN UP MODAL- hidden -->
<div class="modal fade" id="accomModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">Add Accommodation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <!-- Form  header -->
                <div class="create-an-account">
                    <Form class ="regiForm" action = "Function/SM_Process.php" method = "post">

                       <!-- Accommodation Address -->
                        <div class="form-row">

                            <div class="form-group col-md-3">
                                <label class="font-weight-bold">Accommodation Address</label>
                                <input class="form-control" placeholder="Address" id="accomm_address" name="accomm_address"  required>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="font-weight-bold">Suburb</label>
                                <input class="form-control" placeholder="Suburb" id="accomm_suburb" name="accomm_suburb"  required>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="font-weight-bold">State</label>
                                <select class="form-control" id="state" name="state" required>
                                    <option value="" selected disabled>Select your state</option>
                                    <option value="">ACT</option>
                                    <option value="">Northern Australia</option>
                                    <option value="">NSW</option>
                                    <option value="">South Australia</option>
                                    <option value="">Tasmania</option>
                                    <option value="">Victoria</option>
                                    <option value="">Western Australia</option>
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="font-weight-bold">Postcode</label>
                                <input class="form-control" placeholder="Postcode" id="accomm_postcode" name="accomm_postcode"  required>
                            </div>
                            
                        </div>
        
                        <!-- Upload image -->
                        <div class = "form-row">
                            <div class ="form-group col-md-4 align-middle">
                                <label for="uploadImage"class="font-weight-bold">Image</label>
                                <input type="file" class="form-control-file" id="accomm_image" name="accomm_image">                  
                            </div>
                        </div>

                         <!-- Price--> <!-- Bathroom--> <!-- Room--> <!-- Garage-->
                        <div class="form-row">

                            <div class ="form-group col-md-3 col-sm-3 align-middle">
                                <label class="font-weight-bold">Price</label>
                                <input class="form-control" placeholder="Price" id="accomm_price" name="accomm_price" required>
                            </div>

                            <div class ="form-group col-md-3 col-sm-3">
                                <label class="font-weight-bold">Bathroom</label>
                                <input class="form-control" type ="number" id="accomm_bathroom" name="accomm_bathroom" required>
                            </div>

                            <div class ="form-group col-md-3 col-sm-3">
                                <label class="font-weight-bold">Room</label>
                                <input class="form-control" type ="number" id="accomm_room" name="accomm_room" required>
                            </div>

                            <div class ="form-group col-md-3 col-sm-3">
                                <label class="font-weight-bold">Garage</label>
                                <input class="form-control" type ="number" id="accomm_garage" name="accomm_garage" required>
                            </div>
                        </div> 


                        <!-- Smoking --> <!-- Pet-->  <!-- Internet-->
                        <div class="form-row">
                            <div class ="form-group col-md-3 col-xm-3 align-middle ">
                                <label class="font-weight-bold">Smoking</label><br>
                                <input  type="radio" id="accomm_smoking" name = "accomm_smoking" value = "1" >Yes
                                <input  type="radio" id="accomm_smoking" name = "accomm_smoking" value = "0" >No
                            </div>

                            <div class ="form-group col-md-3 col-xm-3 align-middle ">
                                <label class="font-weight-bold">Pet</label><br>
                                <input  type="radio" id="accomm_pet" name = "accomm_pet" value = "1" >Yes
                                <input  type="radio" id="accomm_pet" name = "accomm_pet" value = "0" >No
                            </div>

                            <div class ="form-group col-md-3 col-xm-3 align-middle ">
                                <label class="font-weight-bold">Internet</label><br>
                                <input  type="radio" id="accomm_internet" name = "accomm_internet" value = "1" >Yes
                                <input  type="radio" id="accomm_internet" name = "accomm_internet" value = "0" >No
                            </div>
                       
                        </div> 
                        

                        <!-- House Rate-->
                        
                        <div class="form-row">
                            <div class ="form-group col-md-3">
                                <label class="font-weight-bold">House rate</label>
                                <input class="form-control" placeholder="House rate" id="accomm_houseRate" name="accomm_houseRate" required>
                            </div>
                            <div class ="form-group col-md-3">
                                <label class="font-weight-bold">Host ID</label>
                                <input class="form-control" placeholder="Host ID" id="host_ID" name="host_ID" required>
                            </div>
                        </div> 

                        <!-- Availability -->

                        <!-- Date From To-->
                        <div class="form-row">
                            <div class ="form-group col-md-3 col-xm-3 align-middle ">
                                <label class="font-weight-bold">Available</label><br>
                                <input  type="radio" id="accomm_availibility" name = "accomm_availibility" value = "1" >Yes
                                <input  type="radio" id="accomm_availibility" name = "accomm_availibility" value = "0" >No
                            </div>

                            <div class ="form-group col-md-3 col-xm-3 align-middle ">
                                <label>From</label>
                                <input class="form-control" type="date" id="accomm_available_date_from" name="accomm_available_date_from" required>
                            </div>

                            <div class ="form-group col-md-3 col-xm-3 align-middle ">
                                <label>To</label>
                                <input class="form-control" type="date" id="accomm_available_date_to" name="accomm_available_date_to" required>
                            </div>
                        </div>


                        <button class="btn btn-primary float-right" type="submit" id="" name="addAccommodation">Add Accommodation</button>
                        <p id="msg"></p>

                    </Form>
                </div>
                <!-- End form -->
            </div>
            <!-- End class="modal body -->

            <div class="modal-footer"></div>
        </div>
        <!-- End class="modal-content" -->
    </div>
    <!-- End class="modal-dialog" -->
</div> 
<!-- End registration form -->