<!-- SIGN IN MODAL -->
<div class="modal fade" id="destinationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header">
                <h5 class="modal-title">Add Popular Destinations</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="login">
                    <Form  enctype ="multipart/form-data" action = "Function/SM_Process.php" method = "post">
        
                        <!-- Destination Name-->
                        <div class="form-row">
                            <div class ="form-group col-md-12">
                                <label>Destination Name</label>
                                <input class="form-control" placeholder="Enter destination name" id="desti_name" name="desti_name"  required>
                            </div>
                        </div>
        
                        <!-- Upload image -->
                        <div class = "form-row">
                            <div class ="form-group col-md-12">
                                <label for="uploadImage">Image</label>
                                <input type="file" class="form-control-file" id="desti_image" name="desti_image">                  
                            </div>
                        </div>

                        <!-- Airline-->
                        <div class="form-row">
                            <div class ="form-group col-md-12">
                                <label>Airline</label>
                                <input class="form-control" placeholder="Airline" id="desti_airline" name="desti_airline" required>
                            </div>
                        </div>  
                        
                        <button class="btn btn-primary" type="submit" id="login_button" name="addDestination">Add Destination</button>
                        <p id="desti_msg"></p>
                    </Form>
                </div>
                <!-- End form -->              
            </div>
            <!-- End class="modal body -->

            <div class="modal-footer"></div>
        </div>
        <!-- End class="modal-content" -->
    </div>
    <!-- End class="modal-dialog" -->
</div>