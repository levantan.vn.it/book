<!-- EDIT MODAL -->
<div class="modal fade" id="editDestinationModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header">
                <h5 class="modal-title">Edit Popular Destinations</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="">
                    <Form id="editDestinationForm" action = "Function/SM_Process.php" method = "post">
        
                        <!-- Destination Name-->
                        <div class="form-row">
                            <div class ="form-group col-md-12">
                                <label>Destination Name</label>
                                <input class="form-control" type="text" id="desti_name" name="desti_name"  required>
                            </div>
                        </div>
        
                        <!-- Upload image -->
                        <div class = "form-row">
                            <div class ="form-group col-md-12">
                                <label>Image</label>
                                <input type="file" class="form-control-file" id="desti_image" name="desti_image">                  
                            </div>
                        </div>

                        <!-- Airline-->
                        <div class="form-row">
                            <div class ="form-group col-md-12">
                                <label>Airline</label>
                                <input class="form-control"type="text" id="desti_airline" name="desti_airline" required>
                            </div>
                        </div>  
                        
                        <button type="button" class="btn btn-danger float-left" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary float-right" type="submit" id="editDestination" name="editDestination">Save</button>
                        <p id="desti_msg"></p>
                    </Form>
                </div>
                <!-- End form -->              
            </div>
            <!-- End class="modal body -->

            <div class="modal-footer"></div>
        </div>
        <!-- End class="modal-content" -->
    </div>
    <!-- End class="modal-dialog" -->
</div>

<script>

    $(document).ready(function()
    {
        $('.open-edit').on('click', function()
        {
            var edit = $(this).attr("id");
            $('#editDestinationModal').modal('show');
            console.log(edit);
            $.ajax(
                {
                    url: "Function/SM_Process.php",
                    method: "POST",
                    data: 
                    {
                        edit: edit
                    },
                    dataType: "json",
                    success: function(data) 
                    {
                        $('#desti_name').val(data.desti_name);
                        $('#desti_image').val(data.desti_image);
                        $('#desti_airline').val(data.desti_airline);

                        $('#editDestinationModal').modal('show');
                    }
                });
        });
    });

</script>