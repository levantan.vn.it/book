<?php
        include "Fixed_Head.php";
    ?>
    <!-- Add CSS style -->
    <link rel="stylesheet" href="./Style/Dashboard_SystemManager.css">
    
    <?php
        include "Fixed_Navigation_Bar.php";
    ?>        

  <!-- START SYSTEM MANAGER -->
      
    <!-- Sidebar Menu -->
    <div class="container-fluid">
      <div >
            <!-- sub Nav tabs -->
        <div class="row mt-5 p-1">
          <div class=" col-12">           
            <ul class="nav nav-pills" id="myTab" role="tablist">
              <li class="nav-item col-sm-2">
                <a class="nav-link active text-center font-weight-bold btn btn-primary" id="house-tab" data-toggle="pill" href="#house" role="tablist" aria-controls="house" aria-selected="false" >Accommodation List</a>
              </li>
              <li class="nav-item col-sm-2">
                <a class="nav-link text-center font-weight-bold btn btn-primary" id="host-pill" data-toggle="pill" href="#host" role="tablist" aria-controls="host" aria-selected="false">Host List</a>
              </li>
              <li class="nav-item col-sm-2">
                <a class="nav-link text-center font-weight-bold btn btn-primary" id="client-pill" data-toggle="pill" href="#client" role="tablist" aria-controls="client" aria-selected="false">Client List</a>
              </li>
              <li class="nav-item col-sm-2">
                <a class="nav-link text-center font-weight-bold btn btn-primary" id="desti-pill" data-toggle="pill" href="#destination" role="tablist" aria-controls="destination" aria-selected="true">Destination List</a>
              </li>
            </ul>       
        </div>
      </div>
        <!--END Sidebar Menu --> 

          
            <!-- Main (Page content) -->
          <main role="main" class="col-sm-12 col-md-12 col-lg-12">
            <!-- Tab Content -->
            <div class="tab-content">

               <!-- House List -->
                <div class="tab-pane active" id="house" role="tabpanel" aria-labelledby="house-tab">
                <div id = houseList>  
             
            <h3><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/><path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/> </svg> Accommodation List  </h3>
                <hr>
              
                <div class="table-responsive">
                <button class = "btn btn-primary float-right" data-toggle="modal" data-target="#accomModal" >Add Accommodation </button>

                  <!-- Accommodation table -->
                  <table class="table text-center table-bordered table-striped">
                    <thead >
                      <th class="align-middle">ID</th>
                      <th class="align-middle">Address</th>
                      <th class="align-middle">Image</th>

                      <th class="align-middle">Price</th>
                      <th class="align-middle">Bathroom</th>
                      <th class="align-middle">Room</th>
                      <th class="align-middle">Garage</th>

                      <th class="align-middle">Smoking</th>
                      <th class="align-middle">Pet</th>
                      <th class="align-middle">Internet</th>

                      <th class="align-middle">House Rate  </th>
                      <th class="align-middle">Host Rate</th>

                      <th class="align-middle">Availability</th>
                      <th class="align-middle">From</th>
                      <th class="align-middle">To</th>
                      <th class="align-middle">Host ID</th>

                      <th class="align-middle" colspan = "2">Action</th>
                    </thead>
                    <tbody >

                      <?php

                      include 'Function/Function_DataConn.php';
                      $sql = "SELECT * FROM accommodation";
                      $result = $mysqli->query($sql);

                      while($row = mysqli_fetch_array($result))
                      {
                        ?>
                        <tr>
                          <td class="align-middle"><?php echo $row ['accomm_ID'] ;?></td>
                          <td class="align-middle"><?php echo $row['accomm_address']; echo', '; echo $row['accomm_suburb']; echo', '; echo $row['accomm_state']; echo', '; echo $row['accomm_postcode'];  ?></td>
                          <td class="align-middle"><?php echo '<img height="100px" width="100px" src="data:image/jpeg;base64,'.base64_encode( $row['accomm_image'] ).'"/>'; ?></td>
                         
                          <td class="align-middle"><?php echo'$'; echo $row['accomm_price']; echo' per week';?></td>
                          <td class="align-middle"><?php echo $row['accomm_bathroom']; echo' bathroom(s)';?></td>
                          <td class="align-middle"><?php echo $row['accomm_room']; echo' room(s)';?></td>
                          <td class="align-middle"><?php echo $row['accomm_garage']; echo' garage(s)';?></td>

                          <td class="align-middle"><?php if($row['accomm_smoking']=="1"){echo 'Yes';} else {echo 'No';}?></td>
                          <td class="align-middle"><?php if($row['accomm_pet']=="1"){echo 'Yes';} else {echo 'No';}?></td>
                          <td class="align-middle"><?php if($row['accomm_internet']=="1"){echo 'Yes';} else {echo 'No';}?></td>

                          <td class="align-middle"><?php echo $row['accomm_houseRate'];?></td>
                          <td class="align-middle"><?php echo $row['host_rate'];?></td>

                          <td class="align-middle"><?php if($row['accomm_availibility']=="1"){echo 'Yes';} else {echo 'No';}?></td>
                          <td class="align-middle"><?php echo $row['accomm_available_date_from'];?></td>
                          <td class="align-middle"><?php echo $row['accomm_available_date_to'];?></td>
                          <td class="align-middle"><?php echo $row['host_ID'];?></td>

                          <td class="align-middle"><a href = #accomModal class="btn btn-dark open-edit" id="<?= $row['accomm_ID']; ?>">Edit</a></td>
                          <td class="align-middle"><a href="Function/SM_Process.php?deleteAccommodation=<?php echo $row['accomm_ID']; ?>" class = "btn btn-danger"> Delete</a></td>
                        </tr>

                      <?php 
                      }; 
                      ?>

                    </tbody>
                  </table>
                  
                </div>
            </div>
        </div>
                <!-- END House List -->
                
                <!-- Host List -->
              <div class="tab-pane" id="host" role="tabpanel" aria-labelledby="host-tab">         
            <div id = userList>        
            <h3><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16"><path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/></svg> Host List</h3>
                <hr>
                <div class="d-flex justify-content-end" id= addUser>
   
                </div>
              
                <div class="table-responsive">
                <button class = "btn btn-primary float-right" data-toggle="modal" data-target="#regiModal" >Add Host </button>
                
                <!-- Host table -->
                <table class="table text-center table-bordered table-striped">
                    <thead>
                      <th>ID</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Address</th>
                      <th>Suburb</th>
                      <th>State</th>
                      <th>Post Code</th>
                      <th>ABN Number</th>
                      <th colspan = "2">Action</th>
                    </thead>
                    <tbody>

                      <?php

                      include 'Function/Function_DataConn.php';
                      $sql = "SELECT * FROM host";
                      $result = $mysqli->query($sql);

                      while($row = mysqli_fetch_array($result))
                      {
                        ?>
                        <tr >
                          <td class="align-middle"><?php echo $row ['host_ID'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_firstName'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_lastName'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_email'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_phoneNumber'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_address'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_suburb'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_state'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_postcode'] ;?></td>
                          <td class="align-middle"><?php echo $row ['host_ABN'] ;?></td>

                          <td class="align-middle"><a href = # class="btn btn-dark open-edit" id="<?= $row['host_ID']; ?>">Edit</a></td>
                          <td class="align-middle"><a href="Function/SM_Process.php?deleteHost=<?php echo $row['host_ID']; ?>" class = "btn btn-danger"> Delete</a></td>
                        </tr>

                      <?php 
                      }; 
                      ?>

                    </tbody>
                  </table>
                </div>
            </div>
        </div>
             <!--END Host List -->
                
            <!-- Client List -->
                
              <div class="tab-pane" id="client" role="tabpanel" aria-labelledby="client-tab">             
              <div id = reviewList>
                <h3 ><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16"><path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/><path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/></svg> Client List</h3>
                <hr>        
                <div class="table-responsive">

                <button class = "btn btn-primary float-right" data-toggle="modal" data-target="#regiModal" >Add Client </button>
                <!-- Client table -->
                <table class="table text-center table-bordered table-striped">
                    <thead>
                      <th>ID</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Address</th>
                      <th>Suburb</th>
                      <th>State</th>
                      <th>Post Code</th>
                      <th colspan = "2">Action</th>
                    </thead>
                    <tbody>

                      <?php

                      include 'Function/Function_DataConn.php';
                      $sql = "SELECT * FROM client";
                      $result = $mysqli->query($sql);

                      while($row = mysqli_fetch_array($result))
                      {
                        ?>
                        <tr >
                          <td class="align-middle"><?php echo $row ['client_ID'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_firstName'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_lastName'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_email'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_phoneNumber'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_address'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_suburb'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_state'] ;?></td>
                          <td class="align-middle"><?php echo $row ['client_postcode'] ;?></td>

                          <td class="align-middle"><a href = #editDestinationModal class="btn btn-dark open-edit" id="<?= $row['client_ID']; ?>">Edit</a></td>
                          <td class="align-middle"><a href="Function/SM_Process.php?deleteClient=<?php echo $row['client_ID']; ?>" class = "btn btn-danger"> Delete</a></td>
                        </tr>

                      <?php 
                      }; 
                      ?>

                    </tbody>
                  </table>
                </div>
              </div>
              </div>
             <!--END Client List -->


              <!-- Destination List -->
                
              <div class="tab-pane" id="destination" role="tabpanel" aria-labelledby="destination-tab">             
              <div id = destinationList>
                <h3><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16"><path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/><path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/></svg> Destination List</h3>
                <hr>        
                <div class="table-responsive">
                  
                  <button class = "btn btn-primary float-right" data-toggle="modal" data-target="#destinationModal" >Add Destination </button>

                  <!-- Destination table -->
                  <table class="table text-center table-bordered table-striped">
                    <thead>
                      <th>ID</th>
                      <th>Destination</th>
                      <th>Image</th>
                      <th>Airline</th>
                      <th colspan = "2">Action</th>
                    </thead>
                    <tbody >

                      <?php

                      include 'Function/Function_DataConn.php';
                      $sql = "SELECT * FROM destination";
                      $result = $mysqli->query($sql);

                      while($row = mysqli_fetch_array($result))
                      {
                        ?>
                        <tr>
                          <td class="align-middle"><?php echo $row ['desti_ID'] ;?></td>
                          <td class="align-middle"><?php echo $row['desti_name'];?></td>

                          <td class="align-middle"><?php echo '<img height="100px" width="200px" src="data:image/jpeg;base64,'.base64_encode( $row['desti_image'] ).'"/>'; ?></td>

                          <td class="align-middle"><?php echo $row['desti_airline'];?></td>
                          <td class="align-middle"><a href = #editDestinationModal class="btn btn-dark open-edit" id="<?= $row['desti_ID']; ?>">Edit</a></td>
                          <td class="align-middle"><a href="Function/SM_Process.php?deleteDestination=<?php echo $row['desti_ID']; ?>" class = "btn btn-danger"> Delete</a></td>
                        </tr>

                      <?php 
                      }; 
                      ?>

                    </tbody>
                  </table>

                </div>
              </div>
              </div>
             <!--END destination List -->
                
            </div>
              <!--END Tab Content -->
          </main>
        <!-- END Main -->
      </div>
    </div>

    <?php
        // include "Fixed_Footer.php"; 
        include ('SM_Edit_Destination.php');
        include ('SM_Add_Destination.php');
        include ('SM_Add_Accommodation.php');
    ?>

  </body>   
</html>