    <?php
        include 'Function_Session.php';
        if(empty($_SESSION['session_type']) || empty($_SESSION['session_id']) || $_SESSION['session_type'] != 'host'){
            header("Location: ".$base_url);
        }
        include "template/Fixed_Head.php";
        include "template/Fixed_Navigation_Bar.php";
    ?>
    <!-- Add CSS style -->
    <link rel="stylesheet" href="./Style/Dashboard_Host.css">


<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Participant</title>
    <link rel="stylesheet" href="style.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  </head>

  <body> 
    <!-- START CODE FOR DASHBOARD FOR HOST -->
    <div style="background: url('https://images.unsplash.com/photo-1549180030-48bf079fb38a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1482&q=80') bottom center/ cover no-repeat;">

    <!-- Total view of houses being listed -->

        <div class="container-fluid px-1 py-5 mx-auto">

            <div class="row justify-content-center">

                <div class="col-xl-7 col-lg-8 col-md-10 col-12 text-center mb-5">

                    <!-- Number of houses being listed -->
                    <div class="justify-content-center">
                        <div class="alert alert-info">
                            <span class="badge badge-success" href="#">4</span> houses are being listed under your account
                            <div>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#accomModal" >Add Accommodations</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="table-responsive">

                    <!-- Accommodation table -->
                    <table class="table text-center table-bordered table-striped">
                        <thead >
                        <th class="align-middle">ID</th>
                        <th class="align-middle">Address</th>
                        <th class="align-middle">Image</th>

                        <th class="align-middle">Price</th>
                        <th class="align-middle">Bathroom</th>
                        <th class="align-middle">Room</th>
                        <th class="align-middle">Garage</th>

                        <th class="align-middle">Smoking</th>
                        <th class="align-middle">Pet</th>
                        <th class="align-middle">Internet</th>

                        <th class="align-middle">House Rate  </th>
                        <th class="align-middle">Host Rate</th>

                        <th class="align-middle">Availability</th>
                        <th class="align-middle">From</th>
                        <th class="align-middle">To</th>
                        <th class="align-middle">Host ID</th>

                        <th class="align-middle" colspan = "3">Action</th>
                        </thead>
                        <tbody >

                        <?php

                        include 'Function/Function_DataConn.php';
                        $sql = "SELECT * FROM accommodation";
                        $result = $mysqli->query($sql);

                        while($row = mysqli_fetch_array($result))
                        {
                            ?>
                            <tr>
                            <td class="align-middle"><?php echo $row ['accomm_ID'] ;?></td>
                            <td class="align-middle"><?php echo $row['accomm_address']; echo', '; echo $row['accomm_suburb']; echo', '; echo $row['accomm_state']; echo', '; echo $row['accomm_postcode'];  ?></td>
                            <td class="align-middle"><?php echo '<img height="100px" width="100px" src="data:image/jpeg;base64,'.base64_encode( $row['accomm_image'] ).'"/>'; ?></td>
                            
                            <td class="align-middle"><?php echo'$'; echo $row['accomm_price']; echo' per week';?></td>
                            <td class="align-middle"><?php echo $row['accomm_bathroom']; echo' bathroom(s)';?></td>
                            <td class="align-middle"><?php echo $row['accomm_room']; echo' room(s)';?></td>
                            <td class="align-middle"><?php echo $row['accomm_garage']; echo' garage(s)';?></td>

                            <td class="align-middle"><?php if($row['accomm_smoking']=="1"){echo 'Yes';} else {echo 'No';}?></td>
                            <td class="align-middle"><?php if($row['accomm_pet']=="1"){echo 'Yes';} else {echo 'No';}?></td>
                            <td class="align-middle"><?php if($row['accomm_internet']=="1"){echo 'Yes';} else {echo 'No';}?></td>

                            <td class="align-middle"><?php echo $row['accomm_houseRate'];?></td>
                            <td class="align-middle"><?php echo $row['host_rate'];?></td>

                            <td class="align-middle"><?php if($row['accomm_availibility']=="1"){echo 'Yes';} else {echo 'No';}?></td>
                            <td class="align-middle"><?php echo $row['accomm_available_date_from'];?></td>
                            <td class="align-middle"><?php echo $row['accomm_available_date_to'];?></td>
                            <td class="align-middle"><?php echo $row['host_ID'];?></td>

                            <td class="align-middle"><a class="btn btn-dark open-edit" id="<?= $row['accomm_ID']; ?>">Edit</a></td>
                            <td class="align-middle"><a href="Function/SM_Process.php?delete=<?php echo $row['accomm_ID']; ?>" class = "btn btn-danger"> Delete</a></td>
                            <td class="align-middle"><button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"> Comment </button></td>
                            </tr>

                        <?php 
                        }; 
                        ?>

                        </tbody>
                    </table>
        </div>         



    <!--  review table when clicking on comment -->

                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Review Summary</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">

                        <table class="table text-center table-bordered table-striped">
                        <thead >

                        <th class="align-middle">ID</th>
                        <th class="align-middle">Review Tittle</th>
                        <th class="align-middle">Description</th>
                        <th class="align-middle">Rating</th>

                        </thead>
                        <tbody >

                        <?php

                        include 'Function/Function_DataConn.php';
                        $sql = "SELECT * FROM Review";
                        $review = $mysqli->query($sql) or die(mysqli_error($mysqli));

                        while($row = mysqli_fetch_array($review))
                        {
                            ?>
                            <tr>
                            
                            <td class="align-middle"><?php echo $row['review_ID'];;?></td>
                            <td class="align-middle"><?php echo $row['review_title'];?></td>
                            <td class="align-middle"><?php echo $row['description'];?></td>
                            <td class="align-middle"><?php echo $row['rating'];?></td>
                            </tr>

                        <?php 
                        }; 
                        ?>

                        </tbody>
                    </table>


      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

    <!-- end of review table when clicking on comment -->

    <div class="container-fluid px-1 py-5 mx-auto">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-10 col-12 text-center mb-5">
                <div class="card" id=review-card>
                        Let's check out the latest inbox from customers
                </div>
            </div>
        </div>
    </div>  

    <!-- Inbox table -->

    <div class="table-responsive">

<!-- Accommodation table -->
<table class="table text-center table-bordered table-striped">
    <thead >
    <th class="align-middle">ID</th>
    <th class="align-middle">Message</th>
    <th class="align-middle">client_ID</th>
    </thead>
    <tbody >

    <?php

    include 'Function/Function_DataConn.php';
    $sql = "SELECT * FROM Inbox";
    $result = $mysqli->query($sql) or die(mysqli_error($mysqli));

    while($row = mysqli_fetch_array($result))
    {
        ?>
        <tr>
        <td class="align-middle"><?php echo $row ['inbox_ID'] ;?></td>
        <td class="align-middle"><?php echo $row['Message'];?></td>
        <td class="align-middle"><?php echo $row['client_ID'];?></td>
        
        </tr>

    <?php 
    }; 
    ?>

    </tbody>
</table>
</div> 

    <!-- End of Inbox Table -->
   
        <!-- END DASHBOARD FOR HOST -->
    </div>



<!-- End of Edit -->
    <?php
        include "Fixed_Footer.php"; 
        include "SM_Add_Accommodation.php";
        include "edit.php"

    ?>
       
</body>
</html>