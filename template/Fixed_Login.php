<!-- SIGN IN MODAL -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">Log in</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="login">
                    <Form class ="login_form" id="login_form">
                        <!-- Sign in as a client or a host or a system manager-->
                        <div class = "form-group">
                            <label>Are you loging in as a client, a host or system manager?</label>
                            <select class="form-control col-md-6" name="user_type" required>
                                <option value="" selected disabled>Select your choice</option>
                                <option value="1">Client</option>
                                <option value="2">Host</option>
                                <option value="3">System Manager</option>
                            </select>
                        </div>

                        <!-- Email Address-->
                        <div class="form-row">
                            <div class ="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Enter your email" id="login_email" name="email" required>
                            </div>
                        </div>

                        <!-- Password -->
                        <div class = "form-row">
                            <div class ="form-group col-md-6">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Enter your password" id="login_password" name = "password" required>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit" id="login_button">Log In</button>
                        <p id="login_msg"></p>
                    </Form>
                </div>
                <!-- End form -->
            </div>
            <!-- End class="modal body -->

            <div class="modal-footer"></div>
        </div>
        <!-- End class="modal-content" -->
    </div>
    <!-- End class="modal-dialog" -->
</div>

<script type="text/javascript">
    $('#login_button').click(function(){
        event.preventDefault();
        console.log('button clicked');
        // console.log($("#login_form").serialize());
        // If email or password is em
        if($('#login_email').val() == "" ||$('#login_password').val() == "" )
        {
            $('#login_msg').html('<span style="color:red;">You have to enter your email and password. Please try again.</span>');
        }
        else
        {
            $.ajax(
            {
                url: "Function/Function_Login.php",
                method: "POST",
                data:  $("#login_form").serialize(),
                beforeSend: function ()
                {
                    $('#login_msg').html('<span style="color:green;">Loading process...</span>');
                },
                success: function (response)
                {
                    if(response == 'client')
                    {
                        $('#login_msg').html('<span style="color:green;">We are logging into your client account..Wait a second.. </span>');
                        setTimeout(' window.location.href = "Client.php"; ',500);

                    }
                    else if (response == 'host') {
                        $('#login_msg').html('<span style="color:green;">We are logging into your host account..Wait a second.. </span>');
                        setTimeout(' window.location.href = "Page_Dashboard_Host.php"; ',500);

                    } else if (response == 'system manager') {
                        $('#login_msg').html('<span style="color:green;">We are logging into your system manager account..Wait a second.. </span>');
                        setTimeout(' window.location.href = "Page_Dashboard_SM.php"; ',500);

                    }

                    else
                    {
                        $('#login_msg').html('<span style="color:red;">'+response+'</span>');
                    }
                }
            });
        }
    });
</script>
