<!-- REGISTRATION/ SIGN UP MODAL- hidden -->
<div class="modal fade" id="regiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">Create an account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <!-- Form  header -->
                <div class=create-an-account>
                    <Form class ="regiForm" action = "Function/SM_Process.php" method = "post">

                        <!-- Register as a client or a host -->
                        <div class = "form-group">
                            <label>Are you registering as a client or a host</label>
                            <select class="form-control" id="inputClientHost" name="user_type" required onchange="displayABN('ABN', this)">
                                <option value="0" selected disabled >Select your choice</option>
                                <option value="1">Client</option>
                                <option value="2">Host</option>
                            </select>
                        </div>
        
                        <!-- First Name & Last Name -->
                        <div class="form-row">
                            <div class ="form-group col-md-6">
                                <label for="firstName">First Name</label>
                                <input type="text" class ="form-control" required placeholder="Enter your first name" id="firstName" name ="firstName">
                            </div>
                            <div class ="form-group col-md-6">
                                <label for="firstName">Last Name</label>
                                <input type="text" class ="form-control" required placeholder="Enter your last name" id="lastName" name ="lastName" >
                            </div>
                        </div>
        
        
                        <!-- Email Address & Mobile Number -->
                        <div class="form-row">
                            <div class ="form-group col-md-6">
                                <label>Email</label>
                                <input type="email" class="form-control" required placeholder="Enter your email" id="regiEmail" name ="email">
                            </div>
                            <div class ="form-group col-md-6">
                                <label for="mobileNumber">Mobile Number</label>
                                <input type="text" class="form-control" required placeholder="Enter your mobile number" id="mobileNumber" name ="phoneNumber" title="Phone number must be a 10 digit-number" pattern="(?=.*[0-9]).{10}">
                            </div>
                        </div>
        
                        <!-- Password & Confirm Password -->
                        <div class = "form-row">
                            <div class ="form-group col-md-6">
                                <label >Password</label>
                                <input type="password" class="form-control" required placeholder="Enter your password" id="regiPassword" name ="regiPassword" pattern="(?=.*\d)(?=.*[!@#%])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,12}" name = "password" 
                                title = "Password must contain at least 1 lower case letter, 1 uppercase letter, 1 number, one of following special characters ! @ # $ % and between 6 and 12 characters">
                            </div>
                            <div class ="form-group col-md-6">
                                <label >Confirm Password</label>
                                <input type="password" class="form-control" placeholder="Confirm your password" id="regiConfirmPassword">
                            </div>
                        </div>
                       
                        <!-- Address-->
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" required id="address" name = "address" placeholder="123 Four street">
                        </div>
        
                        <!-- Suburd/City, State & Postcode -->
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="suburb">Suburd/City</label>
                                <Input type="text" class="form-control" required id="suburb" name="suburb" placeholder="Launceston">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="state">State</label>
                                <select class="form-control" id="state" name="state" required>
                                    <option value="" selected disabled>Select your state</option>
                                    <option value="">ACT</option>
                                    <option value="">Northern Australia</option>
                                    <option value="">NSW</option>
                                    <option value="">South Australia</option>
                                    <option value="">Tasmania</option>
                                    <option value="">Victoria</option>
                                    <option value="">Western Australia</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Postcode</label>
                                <Input type="text" class="form-control" id="postcode" name ="postcode" pattern="(?=.*[0-9]).{4}" required >
                            </div>
                        </div>
        
                        <div class="form-row" id ="ABN">
                            <div class="form-group col-md-6">
                                    <label for="ABNNumber">ABN Number</label>
                                    <Input type="text" id="ABNNumber" class="form-control" name="ABNNumber" pattern="(?=.*[0-9]).{11}" title="ABN number must contain 11 digits">
                            </div>
                        </div>
                            
                        <input class="btn btn-primary" type="submit" name ="register" value ="Register">

                        <!-- Error message -->
                        <span class = "container-fluid" id="regiMsg"></span>

                    </Form>
                </div>
                <!-- End form -->
            </div>
            <!-- End class="modal body -->

            <div class="modal-footer"></div>
        </div>
        <!-- End class="modal-content" -->
    </div>
    <!-- End class="modal-dialog" -->
</div> 
<!-- End registration form -->

<script  type="text/javascript">
    
    // 2. Validate input Password/  JQUERy
    $(document).ready(function(){
        // When users click "register" (submit) button, the form with the class ="regiForm" will validate the field.
        $('.regiFormw').on('submit',function(event){
            event.preventDefault();
            //Check if the user sellect host or client yet?
            if($('#inputClientHost option:selected').val()=="")
            {
                $('#regiMsg').html('Please sellect Client or Host!').css('color', 'red');
                return false;
            }
            
            else if($('#inputClientHost option:selected').val()=="1")
            {
                //Check if the confirm password #regiConfirmPassword == #regiPassword
                if($('#regiConfirmPassword').val()!=$('#regiPassword').val())
                {
                    $('#regiMsg').html("Error!:Password does not match").css('color', 'red');
                    return false;
                }
                else
                {
                    alert("Your form has been successfully submited");
                    return true;
                }
            }
            
            // Check if ABN number is entered if host is chosen
            else ($('#inputClientHost option:selected').val()=="2")
            {
                if($('#ABNNumber').val()=="")
                {
                    $('#regiMsg').html("Error!: Please enter your ABN number").css('color', 'red');
                    return false;
                }
                //Check if the confirm password #regiConfirmPassword == #regiPassword
                if($('#regiConfirmPassword').val()!=$('#regiPassword').val())
                {
                    $('#regiMsg').html("Error!:Password does not match").css('color', 'red');
                    return false;
                }
                else
                {
                    alert("Your form has been successfully submited");
                }
            }
        });
    });
</script>