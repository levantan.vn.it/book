
 <!DOCTYPE html>
<html  lang="en" >
    <head>
        <meta charset="utf-8">
        <title>KIT501_Assignment 1_Group 14</title>
        
        <!-- CSS -->
        <link rel="stylesheet" href="../Style/Home_Header_Fixed.css">
        <link rel="stylesheet" href="../Style/Home_Slider.css">
        <link rel="stylesheet" href="../Style/Home_Content.css">
        <!-- <link rel="stylesheet" href="./Style/Dashboard_SystemManager.css"> -->

        <!-- Link font awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

        <!-- Latest Bootstrap compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest Bootstrap compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>