<!-- FOOTER -->
    <div id="footer">
        <div class="footer-infor text-center font-weight-bold">
            Contact us via: unitas.pty@utas.au <br>
            Phone number: 1234567890 <br>
            Address: No 1, Hobart Road, Hobard, Tasmania <br>

        </div>
        <div class="social-list">
            <a href="#"><i class="fab fa-facebook"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-youtube"></i></a>
            <a href="#"><i class="fab fa-pinterest"></i></a>
            <a href="#"><i class="fab fa-twitter-alt"></i></a>
            <a href="#"><i class="fab fa-linkedin"></i></a>
        </div>
    </div>
<!-- END FOOTER-->    

<!-- Javascript -->
<script src="./Javascript/script.js" async defer></script>