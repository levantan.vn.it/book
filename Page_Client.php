<?php
include "Fixed_Head.php";
?>
</head>

<body>
    <div id="header">
        <!-- 1.1. Navigation Bar -->
        <ul id="navBar">
            <li id="UTASName">UniTas Pty. Ltd.</li>
            <li><a href="Page_Home.php">Home</a></li>
        </ul>
    </div>

       <!-- Add CSS style -->
       <link rel="stylesheet" href="./Style/Dashboard_SystemManager.css">

<!-- START CLIENT PAGE -->

    <!-- Sidebar Menu -->
    <div class="container-fluid">
      <div >
            <!-- sub Nav tabs -->
        <div class="row mt-5">
          <div class=" col-12">
            <ul class="nav nav-pills" id="myTab" role="tablist">
              <li class="nav-item col-sm-2">
                <a class="nav-link active text-center font-weight-bold" id="booking-pill" data-toggle="pill" href="#booking" role="tablist" aria-controls="booking" aria-selected="false" >Booking List</a>
              </li>
              <li class="nav-item col-sm-2">
                <a class="nav-link text-center font-weight-bold" id="payment-pill" data-toggle="pill" href="#payment" role="tablist" aria-controls="payment" aria-selected="false">Payment</a>
              </li>
              <li class="nav-item col-sm-2">
                <a class="nav-link text-center font-weight-bold" id="inbox-pill" data-toggle="pill" href="#inbox" role="tablist" aria-controls="inbox" aria-selected="false">Inbox</a>
              </li>
              <li class="nav-item col-sm-2">
                <a class="nav-link text-center font-weight-bold" id="account-pill" data-toggle="pill" href="#account" role="account" aria-controls="account" aria-selected="true">Account Details</a>
              </li>
            </ul>
        </div>
      </div>
        <!--END Sidebar Menu -->

 <!-- Main (Page content) -->
 <main role="main" class="col-sm-12 col-md-12 col-lg-12">
            <!-- Tab Content -->
            <div class="tab-content">
               <!-- House List -->
                <div class="tab-pane active" id="booking" role="tabpanel" aria-labelledby="booking-tab">
                <div id = bookingList>

                <div class="table-responsive">
                  <table class="table table-info">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Host</th>
                        <th scope="col">Address</th>
                        <th scope="col">Bedrooms</th>
                        <th scope="col">Bathrooms</th>
                        <th scope="col">Car Spaces</th>
                        <th scope="col">Smoking</th>
                        <th scope="col">Price</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>ABC Hotel</td>
                        <td>12 West Street, Hobart, Tasmania</td>
                        <td>3</td>
                        <td>2</td>
                        <td>Off-street</td>
                        <td>No</td>
                        <td>$300</td>
                        <td>Not Confirmed</td>
                        <td>
                            <button type="button" class="btn btn-primary">Cancel</button>
                            <button type="button" class="btn btn-danger">Pay</button>
                      </tr>
