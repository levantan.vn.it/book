<!-- GROUP 14: The Hung Le - 593307 | Nhat Minh Truong - 585357 | Mohammad Hamedi - 550019 |Trang Mai - 541853 -->
<?php
    include "template/Fixed_Head.php";
    include "template/Fixed_Navigation_Bar.php";
    include "Function/Function_DataConn.php";
     include "Function/Function_Basic.php";
?>
        
            <div id="slider" style="background: url('Image/Slider/Tasmania_1.jpg') bottom center/ cover no-repeat;">
                <div class="slider-content">
                    <div class="text-heading">
                            Welcome to UniTas
                    </div>
                    <div class="text-description">
                            Are you looking for a place to live in Tasmania? <br>
                            We offer more than 100 hotels, hostels, appartments, studios and guest house all around Tasmania
                    </div>
                </div>

                 <!--Search form  -->
                 <div class="container">

                    <form method="get" id="searchForm">

                        <div class="form-group">
                            <label>Where do you want to go?</label>
                            <div id="searchBar">
                                <input type="search" class="form-control col-md-12 col-sm-12">
                                <!-- <i class="fas fa-map-marker-alt"></i> -->
                            </div>
                        </div>

                        <div class="form-row" id="checkRoom">

                            <div class="form-group col-md-3 col-xs-3">
                                <label>Check-In</label>
                                <input type="date" class="form-control" id="checkInDate">
                            </div>

                            <div class="form-group col-md-3 col-xm3">
                                <label>Check-Out</label>
                                <input type="date" class="form-control" id="checkOutDate">
                            </div>

                            <div class="form-group col-md-2 col-xm-2">
                                <label>Room</label>
                                <input type="number" class="form-control">
                            </div>

                            <div class="form-group col-md-2 col-xs-2">
                                <label>Adult</label>
                                <input type="number" class="form-control ">
                            </div>

                            <div class="form-group col-md-2 col-xs-2">
                                <label>Child</label>
                                <input type="number" class="form-control ">
                            </div>

                        </div>

                        <div class="">
                            <button type="submit" class="btn btn-primary btn-md ">Seach</button>
                            <span id="searchMsg"></span>
                        </div> 
                    </form>
                </div>
                <!-- End searchForm -->

            </div>

            <!-- PART 3. CONTENT -->
             <!-- Start 3.2 Accommodation-->
             <div class="container-fluid">
                <h2 class="text-heading">Accommodations</h2>
                    <div class="row">   

                    <?php
                        $sql = "SELECT * FROM accommodation";
                        $result = $mysqli->query($sql);

                        while($row = mysqli_fetch_array($result))
                        {
                    ?>
                        <!-- Accommodation 1 -->
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6  ">
                            <div class="accommodation-content ">
                                <div class="accommodation-place">
                                    <div class="place-img" style="background-image:url(<?php echo view_image($row['accomm_image'])?>)"> </div>
                                </div>
                                <div class="accommodation-rating">
                                    <?php echo $row['accomm_houseRate']; echo '/5';?>
                                </div>
                                <a class="accommodation-location col-lg-3 col-sm-6"><?php echo $row['accomm_address'];?></a>

                                <div class="accommodation-info">
                                    <i class="accommodation-content-icon fas fa-map-marker-alt"></i>
                                    <span class="accommodation-text"><?php echo $row['accomm_suburb']; echo ' ,'; echo $row['accomm_state'];?></span>
                                </div >

                                <div>
                                    <div class="font-weight-bold row">
                                        <span class="col-sm-5 col-lg-6 display-5 accommodation-price"><?php echo '$'; echo $row['accomm_price']; echo '/week'; ?></span>
                                        <a data-toggle="modal" data-target="#loginModal" class="col-sm-5 bg-info m-2 text-center text-dark">Log in to book</a>
                                    </div>
                                </div>
                                  
                                <div class="flex-sm-row row">
                                    <div class="col-sm-2 col-xm-3 pl-3">
                                        <span><i class="fas fa-bed"></i></span>
                                        <span><?php echo $row['accomm_room'];?></span>
                                    </div>
                                    <div class="col-sm-2 col-xm-3"> 
                                        <span><i class="fas fa-bath"></i></span>
                                        <span><?php echo $row['accomm_bathroom'];?></span>
                                    </div>
                                    <div class="col-sm-2 col-xm-3">
                                        <span><i class="fas fa-smoking-ban"></i><?php if($row['accomm_smoking']=="1"){echo 'Yes';} else {echo 'No';}?></span>
                                    </div>
                                    <div class="col-sm-4 col-xm-6">
                                        <span><i class="fas fa-parking"></i></span>
                                        <span><?php echo $row['accomm_garage'];?></span>
                                    </div>
                                </div>

                                <div class="font-weight-bold bg-primary text-center">
                                    <div class=" p-2">
                                        <?php 
                                            $today = date('Y-m-d');
                                            if($row['accomm_available_date_from']<= $today){echo 'Available';} else {echo'Not Available'; } 
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php 
                        }; 
                    ?>
                      
                <!-- End accommodation -->


                <!-- Start 3.1. Top Destination -->
                <div class="container-fluid mt-5 mb-5">
                    <h2 class="text-heading">Top Destinations</h2>
                    <div class="container-fluid">

                        <?php
                            $query2="SELECT * FROM destination";
                            $result2 = $mysqli->query($query2);
                        ?>

                        <div class="row">   

                        <!-- Destination loop -->
                        <?php
                            $sql = "SELECT * FROM destination";
                            $result = $mysqli->query($sql);
                            while($row = mysqli_fetch_array($result2))
                            {
                        ?>

                                <!-- Destination -->
                                <div class="col-lg-3 col-sm-6">
                                    <a class="place-content">
                                        <div class="place-wrapper">
                                            <div class="place-img" style="background-image:url(<?php echo view_image($row['desti_image'])?>)"></div>

                                        </div>
                                        <div class="place-info">
                                            <h3 class="place-name "><?php echo $row['desti_name'];?></h3>
                                        </div>
                                        <div class="place-info">
                                            <span class="place-airplane"><?php echo $row['desti_airline'];?></span>
                                            <i class="place-icon fa fa-caret-right"></i>
                                        </div>
                                    </a>
                                </div>
                            <?php 
                                };
                            ?>
                            <!-- End Destination -->
                        </div>
                        <!-- End row -->

                    </div>
                    <!-- End container-fluid -->
                </div>
                <!-- End 3.1. Top Destination -->

               

            <!-- END PART 3. CONTENT -->

    <?php
        include "template/FIxed_Footer.php";
    ?>   

    </body>
</html>